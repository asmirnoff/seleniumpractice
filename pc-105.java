/*      PC-105 : Verify that user can navigate to Profile page
        Existing user should have access to his/her profile
        Preconditions: User is logged in
        Steps: 1 Click Avatar button in top right corner 2 Click Profile button in pop up window
 */   //User lands on https://deens-master.now.sh/account/profile page /

package selenium;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.chrome.ChromeDriver;
        import java.util.concurrent.TimeUnit;

public class seleniumDemo {
    public static <UrlValidator> void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","src/selenium/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://deens-master.now.sh");
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector("a[href=\"/login\"]")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#email")).sendKeys("asmirnoff");
        driver.findElement(By.cssSelector("#password")).sendKeys("aaaassss");
        driver.findElement(By.cssSelector("[data-testid='loginSubmit']")).click();
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("div[role='listbox']> div:nth-of-type(1) > img")).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("div.menu.transition.left.visible > div:nth-child(2)")).click();
        String Url = driver.getCurrentUrl();
        if (Url.equals("https://deens-master.now.sh/my/profile")) {
            System.out.println("Test successful. This is My Profile page");
        } else {
            System.out.println("Test Failed! Is it not My Profile page");
        }
        driver.quit(); }
}